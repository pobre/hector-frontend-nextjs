import Layout from '@/components/Layout';
import Profile from '@/screens/Profile';
import React from 'react'

const me = () => {
    return (
        <Layout>
            <Profile />
        </Layout>
    );
}

export default me;