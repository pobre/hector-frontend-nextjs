import Layout from '@/components/Layout';
import Cards from '@/screens/Cards';
import React from 'react'

const cards = () => {

    return (
        <Layout>
            <Cards />
        </Layout>
    );
}

export default cards;