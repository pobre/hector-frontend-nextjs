import React, { useState } from 'react'
import { useTheme } from "next-themes"
import { Button } from '@/components/ui/button'
import { toast } from 'sonner'
import Layout from '@/components/Layout'

export default function Home() {

	return (
		<Layout>
			<div className='mx-auto max-w-7xl'>
				Modifica el archivo de <code className="bg-slate-200 px-2 rounded py-0.5">pages/index.js</code>
			</div>
		</Layout>
	)
}
