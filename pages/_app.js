import { ThemeProvider } from '@/components/utils/theme-provider'
import { Toaster } from 'sonner'
import '@/styles/globals.css'

function App({ Component, pageProps }) {
  return (
    <ThemeProvider
      attribute="class"
      defaultTheme="system"
      enableSystem
      disableTransitionOnChange
    >
      <Toaster richColors />
      <Component {...pageProps} />
    </ThemeProvider>
  )
}

export default App