import React from 'react'
import { AvatarFallback, AvatarImage, AvatarUI } from './ui/avatar';
import { DropdownMenu, DropdownMenuContent, DropdownMenuGroup, DropdownMenuItem, DropdownMenuLabel, DropdownMenuSeparator, DropdownMenuShortcut, DropdownMenuTrigger } from './ui/dropdown-menu';
import { User } from 'lucide-react';
import Router from 'next/router';
import { router } from './router/router';
import { useRouter } from 'next/router';
import Link from 'next/link';

const Header = () => {

    const { asPath } = useRouter()

    return (
        <div className='flex items-center justify-between space-x-4 border-b bg-white py-2 px-4'>
            <div>
                <img src="/img/logo.webp" className="w-20"/>
            </div>
            <div className='items-center flex space-x-6'>
                {router.map((route, index)=>(
                    <Link href={route.path} key={index} className={`items-center space-x-2 flex px-3 py-1.5 rounded cursor-pointer duration-200 transition-all ${asPath?.includes(route?.pathname) ? route?.pathname === '/' ? asPath === '/' ? "bg-slate-100 hover:brightness-95" : ''  : "bg-slate-100 hover:brightness-95" : "hover:bg-slate-100"} `}>
                        {route.icon && <route.icon className="w-5" />}
                        <span>
                            {route.name}
                        </span>
                    </Link>
                ))}
            </div>
            <DropdownMenu>
                <DropdownMenuTrigger asChild>
                    <AvatarUI className="cursor-pointer">
                        <AvatarImage src="https://github.com/shadcn.png" alt="@shadcn" />
                        <AvatarFallback>CN</AvatarFallback>
                    </AvatarUI>
                </DropdownMenuTrigger>
                <DropdownMenuContent className="w-56">
                    <DropdownMenuLabel>My Account</DropdownMenuLabel>
                    <DropdownMenuSeparator />
                    <DropdownMenuGroup>
                        <DropdownMenuItem onClick={()=>Router.push('/profile/me')} className="cursor-pointer">
                            <User className="mr-2 h-4 w-4" />
                            <span>Profile</span>
                        </DropdownMenuItem>
                    </DropdownMenuGroup>
                </DropdownMenuContent>
            </DropdownMenu>
            
        </div>
    );
}

export default Header;