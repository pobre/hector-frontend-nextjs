"use client"

import * as React from "react"
import * as AvatarPrimitive from "@radix-ui/react-avatar"

import { cn } from "@/lib/utils"

const SIZES = {   
  'xs': 'h-8 w-8',
  'sm': 'h-10 w-10',
  'md': 'h-12 w-12',
  'lg': 'h-14 w-14',
  'xl': 'h-16 w-16'
}

const AvatarUI = React.forwardRef(({ className, size, ...props }, ref) => (
  <AvatarPrimitive.Root
    ref={ref}
    className={cn(`relative flex ${SIZES?.[size] || SIZES['md'] } shrink-0 overflow-hidden rounded-full`, className)}
    {...props} />
))
AvatarUI.displayName = AvatarPrimitive.Root.displayName

const AvatarImage = React.forwardRef(({ className, ...props }, ref) => (
  <AvatarPrimitive.Image
    ref={ref}
    className={cn("aspect-square h-full w-full", className)}
    {...props} />
))
AvatarImage.displayName = AvatarPrimitive.Image.displayName

const AvatarFallback = React.forwardRef(({ className, ...props }, ref) => (
  <AvatarPrimitive.Fallback
    ref={ref}
    className={cn(
      "flex h-full w-full items-center justify-center rounded-full bg-muted",
      className
    )}
    {...props} />
))
AvatarFallback.displayName = AvatarPrimitive.Fallback.displayName

export { AvatarUI, AvatarImage, AvatarFallback }
