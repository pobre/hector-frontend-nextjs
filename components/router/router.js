import { Home, Spade } from "lucide-react";

export const router = [
    {
        name: 'Inicio',
        pathname: '/',
        path: '/',
        icon: Home,
    },
    {
        name: 'Cartas',
        pathname: 'cards',
        path: '/cards',
        icon: Spade,
    },
]