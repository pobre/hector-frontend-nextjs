import axios from '../middleware'
import {API} from '../API'

export const getCards = () =>{
    return axios.get(
        `${API}/cards`,
        {
            headers:{
                "Content-Type": 'application/json',
                "X-Requested-With": 'XMLHttpRequest',
                "Accept": 'application/json'
            }
        }
    ).then(res=>{
        return res
    }).catch(err=>{
        return err.response
    })
}

export const postCards = (data) =>{
    return axios.post(
        `${API}/cards`, data,
        {
            headers:{
                "Content-Type": 'application/json',
                "X-Requested-With": 'XMLHttpRequest',
                "Accept": 'application/json'
            }
        }
    ).then(res=>{
        return res
    }).catch(err=>{
        return err.response
    })
}