import { Button } from '@/components/ui/button';
import { Input } from '@/components/ui/input';
import Router from 'next/router';
import Link from 'next/link';
import React from 'react'

const Login = () => {
    return (
        <div className="w-full h-screen grid grid-cols-1 lg:grid-cols-2">
            <div className="w-full relative hidden lg:block col-span-1">
                <img src="/img/login.jpg" className="object-cover w-full h-full" />
            </div>
            <div className="w-full h-full justify-center flex items-center col-span-1">
                <div className='w-72 lg:w-96 space-y-6'>
                    <div className="flex justify-center">
                        <img src="/img/logo.webp" className="w-48"/>
                    </div>
                    <div className='space-y-3'>
                        <div>
                            <label className="text-sm text-gray-500 font-semibold">
                                Email
                            </label>
                            <Input
                                placeholder="Write your email"
                            />
                        </div>
                        <div>
                            <label className="text-sm text-gray-500 font-semibold">
                                Contraseña
                            </label>
                            <Input 
                                placeholder="********"
                                type="Password"
                            />
                        </div>
                        <div className='flex justify-end'>
                            <Button className="w-full" onClick={()=>Router.push('/')} >
                                Login
                            </Button>
                        </div>
                        <div className='flex justify-center'>
                            <Link href="/register">
                                ¿No tienes una cuenta?
                            </Link>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    );
}

export default Login;