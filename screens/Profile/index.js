import { AvatarFallback, AvatarImage, AvatarUI } from '@/components/ui/avatar';
import { Button } from '@/components/ui/button';
import { Card } from '@/components/ui/card';
import { Progress } from '@/components/ui/progress';
import { Globe, Instagram, Twitter } from 'lucide-react';
import React from 'react'

const Profile = () => {
    return (
        <div className="grid grid-cols-1 lg:grid-cols-2 mx-auto max-w-7xl w-full gap-6">
            <div className="col-span-1 space-y-6">
                <Card className="p-6 space-y-4">
                    <div className="space-y-2">
                        <div className="justify-center flex">
                            <AvatarUI className="cursor-pointer w-40 h-40">
                                <AvatarImage src="https://github.com/shadcn.png" alt="@shadcn" />
                                <AvatarFallback>CN</AvatarFallback>
                            </AvatarUI>
                        </div>
                        <div className='text-2xl text-slate-800 font-bold text-center'>
                            Hector El Father
                        </div>
                    </div>
                    <div className='justify-center flex text-lg text-slate-500'>
                        <span>
                            Lorem ipsum dolor sit amet, consectetur adipiscing el...
                        </span>
                    </div>
                    <div className='justify-center flex text-lg text-slate-500 space-x-4'>
                        <Button>
                            Follow
                        </Button>
                        <Button variant="secondary">
                            Message
                        </Button>
                    </div>
                </Card>
                <Card className="p-6 text-lg">
                    <div className='space-y-4'>
                        <div className='flex items-center justify-between'>
                            <div className='items-center flex space-x-2'>
                                <Globe />
                                <span>
                                    Website
                                </span>
                            </div>
                            <span className='text-slate-500'>
                                https://example.com
                            </span>
                        </div>
                        <div className='flex items-center justify-between'>
                            <div className='items-center flex space-x-2'>
                                <Twitter />
                                <span>
                                    Twitter
                                </span>
                            </div>
                            <span className='text-slate-500'>
                                @HectorHater
                            </span>
                        </div>
                        <div className='flex items-center justify-between'>
                            <div className='items-center flex space-x-2'>
                                <Instagram />
                                <span>
                                    Instagram
                                </span>
                            </div>
                            <span className='text-slate-500'>
                                @HectorNudes
                            </span>
                        </div>
                    </div>
                </Card>
            </div>
            <div className="col-span-1 space-y-6">
                <Card className="p-6">
                    <div className=''>
                        <div className='grid grid-cols-3 border-b py-2'>
                            <div className='col-span-1'>
                                Full Name
                            </div>
                            <div className='col-span-2'>
                                Hector ElFather
                            </div>
                        </div>

                        <div className='grid grid-cols-3 border-b py-2'>
                            <div className='col-span-1'>
                                Email
                            </div>
                            <div className='col-span-2'>
                                hector@xxx.com
                            </div>
                        </div>

                        <div className='grid grid-cols-3 py-2'>
                            <div className='col-span-1'>
                                ...
                            </div>
                            <div className='col-span-2'>
                                ...
                            </div>
                        </div>

                    </div>
                </Card>
                <div className='grid grid-cols-1 lg:grid-cols-2 gap-6'>
                    <Card className="p-6">
                        <div className='space-y-4'>
                            <h2 className='text-2xl font-semibold text-slate-800'>
                                Edit Text Here
                            </h2>
                            <div>
                                <label className='text-slate-500 text-lg'>Placeholder</label>
                                <Progress value={67} />
                            </div>
                            <div>
                                <label className='text-slate-500 text-lg'>Placeholder</label>
                                <Progress value={46} />
                            </div>
                            <div>
                                <label className='text-slate-500 text-lg'>Placeholder</label>
                                <Progress value={78} />
                            </div>
                            <div>
                                <label className='text-slate-500 text-lg'>Placeholder</label>
                                <Progress value={12} />
                            </div>
                            
                        </div>
                    </Card>
                    <Card className="p-6">
                        <div className='space-y-4'>
                            <h2 className='text-2xl font-semibold text-slate-800'>
                                Edit Text Here
                            </h2>
                            <div>
                                <label className='text-slate-500 text-lg'>Placeholder</label>
                                <Progress value={22} />
                            </div>
                            <div>
                                <label className='text-slate-500 text-lg'>Placeholder</label>
                                <Progress value={43} />
                            </div>
                            <div>
                                <label className='text-slate-500 text-lg'>Placeholder</label>
                                <Progress value={67} />
                            </div>
                            <div>
                                <label className='text-slate-500 text-lg'>Placeholder</label>
                                <Progress value={11} />
                            </div>
                            
                        </div>
                    </Card>
                </div>
            </div>
        </div>
    );
}

export default Profile;