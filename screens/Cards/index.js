import { Card } from '@/components/ui/card';
import { getCards } from '@/services/cards/cards.service';
import React, { useEffect, useState } from 'react'

const Cards = () => {

    const [cards, setCards] = useState([])
    const [loading, setLoading] = useState(true)

    const handleGetCards = () =>{
        getCards().then(res=>{
            console.log("res cards->", res)
            setLoading(false)
            if(res?.status === 200){
                setCards(res?.data)
            }
        })
    }

    useEffect(() => {
        handleGetCards()
    }, [])
    

    return (
        <div className="grid grid-cols-1 lg:grid-cols-6 mx-auto max-w-7xl w-full gap-6">
            {loading &&
                Array.from({length: 18}, (_, i) => i + 1).map((skeleton)=>(
                    <div className='h-44 rounded bg-slate-200 animate-pulse' />
                ))
            }
            {cards?.map((card, index)=>(
                <Card key={index} className="h-44 flex items-center justify-center ">
                    {card?.name}
                </Card>
            ))}
        </div>
    );
}

export default Cards;